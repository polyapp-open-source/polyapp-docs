package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

var AllPages = []Page{
	{
		Head:   Head{
			Title:       "Polyapp Documentation",
			Description: "Documentation center for Polyapp, a form builder and low code tool.",
		},
		Body:   "",
		htmlPath: "index.html",
		Footer: Footer{},
	},
	{
		Head: Head{
			Title: "System Capabilities | Polyapp",
			Description: "An overview of Polyapp's out-of-the-box capabilities and features.",
		},
		htmlPath: "capabilities.html",
	},
	{
		Head:   Head{
			Title:       "Reporting | Polyapp Fundamentals",
			Description: "Polyapp's reporting helps you view and understand data in your Tasks.",
		},
		Body:   "",
		htmlPath: "fundamentals/reporting.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Bots | Polyapp Fundamentals",
			Description: "",
		},
		Body:   "",
		htmlPath: "fundamentals/bots.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Dashboards | Polyapp Fundamentals",
			Description: "",
		},
		Body:   "",
		htmlPath: "fundamentals/dashboard.html",
		Footer: Footer{},
	},
	// {
	// 	Head:   Head{
	// 		Title:       "Surveys | Polyapp Fundamentals",
	// 		Description: "",
	// 	},
	// 	Body:   "",
	// 	htmlPath: "fundamentals/survey.html",
	// 	Footer: Footer{},
	// },
	{
		Head:   Head{
			Title:       "Personalization | Polyapp Fundamentals",
			Description: "",
		},
		Body:   "",
		htmlPath: "fundamentals/personalization.html",
		Footer: Footer{},
	},
	{
		Head: Head{
			Title: "Deployment | System Administration",
			Description: "",
		},
		Body: "",
		htmlPath: "system-administration/deployment.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Roles | System Administration",
			Description: "",
		},
		Body:   "",
		htmlPath: "system-administration/roles.html",
		Footer: Footer{},
	},
	{
		Head: Head{
			Title: "Troubleshooting | System Administration",
			Description: "",
		},
		Body: "",
		htmlPath: "system-administration/troubleshooting.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Tasks | Polyapp Fundamentals",
			Description: "",
		},
		Body:   "",
		htmlPath: "fundamentals/tasks.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Users | System Administration",
			Description: "",
		},
		Body:   "",
		htmlPath: "system-administration/users.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Development | Polyapp Advanced",
			Description: "",
		},
		Body:   "",
		htmlPath: "advanced/development.html",
		Footer: Footer{},
	},
	{
		Head:   Head{
			Title:       "Transferring | Polyapp Advanced",
			Description: "",
		},
		Body:   "",
		htmlPath: "advanced/transferring.html",
		Footer: Footer{},
	},
	{
		Head: Head{
			Title: "API Integration | Polyapp Advanced",
			Description: "",
		},
		Body: "",
		htmlPath: "advanced/APIintegration.html",
		Footer: Footer{},
	},
	{
		Head: Head{
			Title: "JSON 2 App | Polyapp Advanced",
			Description: "",
		},
		htmlPath: "advanced/json2app.html",
	},
	{
		Head: Head{
			Title: "Microservice Bot Development | Polyapp Advanced",
			Description: "",
		},
		htmlPath: "advanced/MicroserviceBotDevelopment.html",
	},
	// {
	// 	Head: Head{
	// 		Title: "References | Polyapp Advanced",
	// 		Description: "",
	// 	},
	// 	htmlPath: "advances/references.html",
	// },
	{
		Head: Head{
			Title: "Get Started | System Administration",
			Description: "",
		},
		htmlPath: "system-administration/get-started.html",
	},
	{
		Head: Head{
			Title: "Import | Polyapp Fundamentals",
			Description: "",
		},
		htmlPath: "fundamentals/import.html",
	},
}

type Page struct {
	Head Head
	Body string
	htmlPath string
	Footer Footer
}

type Head struct {
	Title string
	Description string
}

type Footer struct {

}

// Generate the website and output the generated site in the "public" folder.
func main() {
	maindir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	outPath := filepath.ToSlash(maindir) + "/public"
	inputPath := filepath.ToSlash(maindir) + "/templates"
	t, err := findAndParseTemplates(inputPath)
	if err != nil {
		panic(err)
	}

	for _, p := range AllPages {
		body, err := ioutil.ReadFile(inputPath + "/" + p.htmlPath)
		if err != nil {
			panic(err)
		}
		p.Body = string(body)
		_ = os.Remove(outPath + "/" + p.htmlPath)
		f, err := os.OpenFile(outPath + "/" + p.htmlPath, os.O_CREATE, 0666)
		if err != nil {
			panic(err)
		}
		err = t.ExecuteTemplate(f, "main", p)
		if err != nil {
			panic(err)
		}
		f.Close()
	}
}

// findAndParseTemplates walks rootDir and all subdirectories. It finds *.html and parses it into a template
// which is placed on a root template which is returned (potentially with an error).
func findAndParseTemplates(path string) (*template.Template, error) {
	cleanRoot := filepath.Clean(path)
	prefixLength := len(cleanRoot) + 1
	root := template.New("")

	err := filepath.Walk(cleanRoot, func(path string, info os.FileInfo, e1 error) error {
		if !info.IsDir() && strings.HasSuffix(path, ".html") {
			if e1 != nil {
				return e1
			}

			b, e2 := ioutil.ReadFile(path)
			if e2 != nil {
				return e2
			}

			name := path[prefixLength:]
			t := root.New(name)
			_, e2 = t.Parse(string(b))
			if e2 != nil {
				return e2
			}
		}
		return nil
	})
	return root, err
}
